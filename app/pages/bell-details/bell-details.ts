import {Modal, Page, NavController, NavParams, Alert} from 'ionic-angular';
import {NgClass} from 'angular2/common';
import {BellData} from '../../providers/bell-data/bell-data';
import {Bell} from '../../models/bell/bell';
import {RingLogModal} from './ring-log/ring-log';
import {AvatarModal} from './avatar/avatar';
import {RingData} from '../../providers/ring-data/ring-data';
import {RingDetailsPage} from '../ring-details/ring-details';
import {BellEditModal} from './bell-edit/bell-edit';

@Page({
  templateUrl: 'build/pages/bell-details/bell-details.html'
})
export class BellDetailsPage {
  selectedBell: any;
  rings: any;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    private bellData: BellData,
    private ringData: RingData) {
    this.nav = nav;
    this.selectedBell = navParams.get('bell');
    ringData.load(this.selectedBell, this.selectedBell.ringing).then((rings) => { this.rings = rings });
  }

  awayToggle(btn: String) {
    // Avoid setting UPDATE if not needed
    if (!(((btn == "out") && (this.selectedBell.away)) || ((btn == "home") && (!this.selectedBell.away)))) {
      let parameters = `{\"away\":\"${!this.selectedBell.away}\",\"sender\":\"mobile\"}`;
      this.bellData.customActionCall("update_away", this.selectedBell, parameters, true)
        .then((bell) => {
          this.selectedBell = bell;
        });
    }
  }

  refresh(refresher) {
    this.bellData.loadOne(this.selectedBell, true).then((bell) => {
      this.selectedBell = bell;
      this.ringData.load(this.selectedBell, true).then((rings) => { this.rings = rings });
      refresher.complete();
    });
  }

  openRingLogModal() {
    let modal = Modal.create(RingLogModal, {rings: this.rings});
    this.nav.present(modal);
  }

  openAvatarModal() {
    let modal = Modal.create(AvatarModal, {bell: this.selectedBell});
    modal.onDismiss(bell => {
      if (bell != null) {
        this.selectedBell = bell;
      }
    });
    this.nav.present(modal);
  }

  openEditModal() {
    let modal = Modal.create(BellEditModal, {bell: this.selectedBell});
    modal.onDismiss(bell => {
      if (bell != null) {
        this.selectedBell = bell;
      }
    });
    this.nav.present(modal);
  }

  openRingDetailsPage() {
    this.nav.push(RingDetailsPage, {ring: this.rings[0], bell: this.selectedBell});
  }

}
