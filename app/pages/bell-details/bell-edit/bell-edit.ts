import {FormBuilder, Validators} from 'angular2/common';
import {Modal, NavController, ViewController, Page, Platform, NavParams, Alert} from 'ionic-angular';
import {ViewChild} from 'angular2/core';
import {BellData} from '../../../providers/bell-data/bell-data';
import {Bell} from '../../../models/bell/bell';

@Page({
  templateUrl: 'build/pages/bell-details/bell-edit/bell-edit.html',
})
export class BellEditModal {
  static get parameters() {
    return [[NavController], [NavParams], [BellData], [FormBuilder], [ViewController]];
  }

  bell: Bell;
  bellForm: any;
  formBuilder: FormBuilder;
  nav: NavController;
  viewCtrl: ViewController;
  bellData: BellData;


  constructor(nav, navParams, bellData, formBuilder, viewController) {
    this.nav = nav;
    this.bell = navParams.get('bell');
    this.formBuilder = formBuilder;
    this.viewCtrl = viewController;
    this.bellData = bellData;

    this.bellForm = formBuilder.group({
      id: [this.bell.id, Validators.required],
      name: [this.bell.name, Validators.compose([Validators.required, Validators.maxLength(30)])],
      welcome_message: [this.bell.welcomeMessage, Validators.compose([Validators.required, Validators.maxLength(30)])],
      coming_message: [this.bell.comingMessage, Validators.compose([Validators.required, Validators.maxLength(30)])],
      come_up_message: [this.bell.comeUpMessage, Validators.compose([Validators.required, Validators.maxLength(30)])],
      no_answer_message: [this.bell.noAnswerMessage, Validators.compose([Validators.required, Validators.maxLength(30)])]
    });
  }

  close() {
    this.viewCtrl.dismiss(null);
  }

  save() {
    if (!this.bellForm.valid) {
      let alert = Alert.create({
        title: 'Watch Out!',
        subTitle: 'Some of the fields have invalid values',
        buttons: ['OK']
      });
      console.log(this.bellForm);
      this.nav.present(alert);
    } else {
      this.bellData.update(this.bell, JSON.stringify(this.bellForm.value), "send_update_request")
      .then((bell) => {
        this.viewCtrl.dismiss(bell);
      });
    }
  }
}
