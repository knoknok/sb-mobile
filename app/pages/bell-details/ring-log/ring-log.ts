import {Modal, NavController, ViewController, Page, Platform, NavParams} from 'ionic-angular';
import {Ring} from '../../../models/ring/ring';

@Page({
  templateUrl: 'build/pages/bell-details/ring-log/ring-log.html',
})

export class RingLogModal {
  rings: Array<Ring>;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController) {
      this.rings = params.get('rings');
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
