import {Modal, NavController, Slides, ViewController, Page, Platform, NavParams} from 'ionic-angular';
import {ViewChild} from 'angular2/core';
import {Bell} from '../../../models/bell/bell';
import {Avatar} from '../../../models/avatar/avatar';
import {BellData} from '../../../providers/bell-data/bell-data';

@Page({
  templateUrl: 'build/pages/bell-details/avatar/avatar.html',
})

export class AvatarModal {
  bell: Bell;
  slideList = Avatar.slideList();
  slideOptions: any;
  @ViewChild('avatarSlider') slider: Slides;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public bellData: BellData) {
      this.bell = params.get('bell');
      this.slideOptions = { initialSlide: this.bell.avatar - 1 };
    }

    close() {
      this.viewCtrl.dismiss(null);
    }

    save() {
      if (this.bell.avatar != this.slider.getIndex() + 1) {
        this.bellData.update(this.bell, `{"avatar":${this.slider.getIndex() + 1}}`)
        .then((bell) => {
            this.viewCtrl.dismiss(bell);
        });
      }
    }

    avatarPath(id: number) {
      return Avatar.avatarPath(id);
    }
  }
