import {Page, NavController, NavParams, Alert} from 'ionic-angular';
import {Ring} from '../../models/ring/ring';
import {RingData} from '../../providers/ring-data/ring-data';
import {Bell} from '../../models/bell/bell';
import {BellData} from '../../providers/bell-data/bell-data';

@Page({
  templateUrl: 'build/pages/ring-details/ring-details.html',
})
export class RingDetailsPage {
  ring: Ring = null;
  bell: Bell = null;
  intervalSec: number = null;
  intervalString: string = null;
  intervalHandle: any = null;

  constructor(public nav: NavController,
    public navParams: NavParams,
    private bellData: BellData,
    private ringData: RingData) {
      this.nav = nav;
      this.ring = navParams.get('ring');
      this.bell = navParams.get('bell');

      this.intervalSec = Math.round((Date.now() - this.ring.ring_on.getTime()) / 1000);
      this.intervalHandle = setInterval(() => {
        let s = this.pad(++this.intervalSec % 60);
        let m = this.pad(Math.floor(this.intervalSec / 60));
        this.intervalString = m + "m:" + s + "s";
      }, 1000);
    }

    pad(val: number) {
      return val > 9 ? val : "0" + val;
    }

    onPageWillLeave() {
      clearInterval(this.intervalHandle);
    }

    discardRing() {
      let alert = Alert.create({
        title: 'Are you sure?',
        message: 'You will not be able to respond to the call.',
        buttons: [
          {
            text: 'Cancel',
            handler: () => {}
          },
          {
            text: 'Yes',
            handler: () => {
              let params = JSON.stringify({ring_id: this.ring.id, message: "Call Discarded", discarded: true});
              this.bellData.customActionCall("respond_to_call_mobile", this.bell, params, true)
              .then((bell: any) => {
                this.nav.popToRoot();
              });
            }
          }
        ]
      });
      this.nav.present(alert);
    }

    respondMessage(message: string) {
      let alert = Alert.create({
        title: 'Repond With Message',
        message: message,
        buttons: [
          {
            text: 'Cancel',
            handler: () => {}
          },
          {
            text: 'Yes',
            handler: () => {
              let params = JSON.stringify({ring_id: this.ring.id, message: message, discarded: false});
              this.bellData.customActionCall("respond_to_call_mobile", this.bell, params, true)
              .then((bell: any) => {
                this.nav.popToRoot();
              });
            }
          }
        ]
      });
      this.nav.present(alert);
    }

    respondDelayMessage() {
      let alert = Alert.create({
        title: 'Repond With Message',
        message: "I am coming in ... minutes",
        inputs: [
          {
            name: 'minutes',
            placeholder: '... minutes'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: () => {}
          },
          {
            text: 'Yes',
            handler: (data) => {
              console.log(data);
              let params = JSON.stringify({ring_id: this.ring.id, message: "I am coming in " + data.minutes + " minutes", discarded: false});
              this.bellData.customActionCall("respond_to_call_mobile", this.bell, params, true)
              .then((bell: any) => {
                this.nav.popToRoot();
              });
            }
          }
        ]
      });
      this.nav.present(alert);
    }

    respondCustomMessage() {
      let alert = Alert.create({
        title: 'Repond With Custom Message',
        message: "Type your message",
        inputs: [
          {
            name: 'message',
            placeholder: 'Custom Message'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: () => {}
          },
          {
            text: 'Yes',
            handler: (data) => {
              let params = JSON.stringify({ring_id: this.ring.id, message: data.message, discarded: false});
              this.bellData.customActionCall("respond_to_call_mobile", this.bell, params, true)
              .then((bell: any) => {
                this.nav.popToRoot();
              });
            }
          }
        ]
      });
      this.nav.present(alert);
    }
  }
