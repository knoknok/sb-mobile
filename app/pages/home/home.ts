import {Page, NavController} from 'ionic-angular';
import {BellData} from '../../providers/bell-data/bell-data';
import {Bell} from '../../models/bell/bell';
import {BellDetailsPage} from '../bell-details/bell-details';
import {RingData} from '../../providers/ring-data/ring-data';

@Page({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  bells: any;

  constructor(
    public nav: NavController,
    private bellData: BellData,
    private ringData: RingData) {
    this.nav = nav;
    this.bellData.load().then((bells) => { this.bells = bells });
  }

  itemTapped(bell) {
    this.nav.push(BellDetailsPage, {bell: bell});
  }

  refresh(refresher) {
    this.bellData.load(true).then((bells) => {
      this.bells = bells;
      refresher.complete();
    });
  }
}
