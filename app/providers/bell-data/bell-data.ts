import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import {Bell} from '../../models/bell/bell';
import 'rxjs/add/operator/map';

@Injectable()
export class BellData {
  data: Array<Bell>;

  constructor(public http: Http) {}

  load(force: boolean = false) {
    if ((this.data) && (!force)) {
      return Promise.resolve(this.data);
    }
    let headers = new Headers();
    headers.append("AnonymousToken", "893d0d88-128c-46b5-8a8f-5a7f4b345210");
    return new Promise(resolve => {
      this.http.get('https://api.backand.com/1/objects/bells', {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          let temp: Array<any> = [];
          data.data.forEach((b) => {
            temp.push(new Bell(b));
          });
          this.data = temp;
          resolve(this.data);
        });
    });
  }

  loadOne(bell: Bell, force: boolean = false) {
    if ((this.data) && (!force)) {
      let i = this.data.indexOf(bell)
      return Promise.resolve(this.data[i]);
    }
    let headers = new Headers();
    headers.append("AnonymousToken", "893d0d88-128c-46b5-8a8f-5a7f4b345210");
    return new Promise(resolve => {
      this.http.get('https://api.backand.com/1/objects/bells/' + bell.id, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          let b = new Bell(data);
          let i = this.data.indexOf(bell);
          this.data[i] = b;
          resolve(this.data[i]);
        });
    });
  }

  update(bell: Bell, updateFields: string, propagate_to: string = "") {
    let headers = new Headers();
    headers.append("AnonymousToken", "893d0d88-128c-46b5-8a8f-5a7f4b345210");
    return new Promise(resolve => {
      this.http.put("https://api.backand.com/1/objects/bells/" + bell.id + "?returnObject=true&deep=false&parameters={\"propagate_to\":\"" + propagate_to + "\"}", updateFields, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          let b = new Bell(data);
          let i = this.data.indexOf(bell);
          this.data[i] = b;
          resolve(this.data[i]);
        });
    });
  }

  customActionCall(action: string, bell: Bell, parameters: string, returnObject: boolean) {
    let headers = new Headers();
    headers.append("AnonymousToken", "893d0d88-128c-46b5-8a8f-5a7f4b345210");
    return new Promise(resolve => {
      this.http.get("https://api.backand.com/1/objects/action/bells/" + bell.id + "?returnObject=" + returnObject + "&name=" + action + "&parameters=" + parameters, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          if (returnObject) {
            let b = new Bell(data);
            let i = this.data.indexOf(bell);
            this.data[i] = b;
            resolve(this.data[i]);
          }
          resolve(data);
        });
    });
  }

}
