import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import {Ring} from '../../models/ring/ring';
import {Bell} from '../../models/bell/bell';
import 'rxjs/add/operator/map';

@Injectable()
export class RingData {
  data: Array<Ring>;

  constructor(public http: Http) {}

  load(bell: Bell, force: boolean = false) {
    if ((this.data) && (this.data[0].bell_id == bell.id) && (!force)) {
      return Promise.resolve(this.data);
    }

    let headers = new Headers();
    headers.append("AnonymousToken", "893d0d88-128c-46b5-8a8f-5a7f4b345210");
    let filter = `?filter=[{"fieldName":"bell_id", "operator":"in", "value":"${bell.id}"}]`;
    let sort = `&sort={"fieldName":"ring_on", "order":"desc"}`;
    return new Promise(resolve => {
      this.http.get('https://api.backand.com/1/objects/rings' + filter + sort, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          let temp: Array<any> = [];
          data.data.forEach((r) => {
            temp.push(new Ring(r));
          });
          this.data = temp;
          resolve(this.data);
        });
    });
  }

  clear() {
    this.data = null;
  }
}
