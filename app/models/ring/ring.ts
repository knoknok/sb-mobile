import {Setting} from '../setting/setting';

export class Ring {
  public id: string = null;
  public bell_id: string = null;
  public ring_on: Date = null;
  public answer: string = null;

  constructor(obj: any){
    this.id = obj.id;
    this.bell_id = obj.bell_id;
    this.ring_on = new Date(obj.ring_on);
    this.answer = obj.answer;
  }

  printRingOn() {
    return this.ring_on.toLocaleDateString("en-us", Setting.getDateOptions());
  }

}
