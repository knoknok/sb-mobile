export class Setting {
  public static getDateOptions(){
    return {
      weekday: "long", year: "numeric", month: "short",
      day: "numeric", hour: "2-digit", minute: "2-digit"
    };
  }
}
