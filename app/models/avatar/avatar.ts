export class Avatar {
  static slideList() {
    return [1, 2, 3, 4, 5, 6];
  }

  static avatarPath(id: number) {
    return "img/avatars/avatar-" + id + ".png";
  }

  static avatarBadgePath(id: number) {
    return "img/avatars/avatar-badge-" + id + ".png";
  }
}
