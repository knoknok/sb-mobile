import {Avatar} from '../avatar/avatar';
import {Setting} from '../setting/setting';

export class Bell {
  public id: string = null;
  public name: string = null;

  public away: boolean = null;
  public ringing: boolean = null;
  public last_ring_on: Date = null;

  public welcomeMessage: string = null;
  public comingMessage: string = null;
  public comeUpMessage: string = null;
  public noAnswerMessage: string = null;

  private particleBellId: string = null;
  private particleDisplayId: string = null;
  private particleAccessToken: string = null;

  public avatar: number = null;

  constructor(obj: any){
    this.id = obj.id;
    this.name = obj.name;
    this.away = obj.away;
    this.ringing = obj.ringing;
    this.last_ring_on = (obj.last_ring_on == null) ? null : new Date(obj.last_ring_on);
    this.welcomeMessage = obj.welcome_message;
    this.comingMessage = obj.coming_message;
    this.comeUpMessage = obj.come_up_message;
    this.noAnswerMessage = obj.no_answer_message;
    this.particleBellId = obj.particle_bell_id;
    this.particleDisplayId = obj.particle_display_id;
    this.particleAccessToken = obj.particle_access_token;
    this.avatar = obj.avatar;
  }

  avatarPath() {
    return Avatar.avatarPath(this.avatar);
  }

  avatarBadgePath() {
    return Avatar.avatarBadgePath(this.avatar);
  }

  printLastRingOn() {
    if (this.last_ring_on) {
      return this.last_ring_on.toLocaleDateString("en-us", Setting.getDateOptions());
    }
    return "No one called yet.";
  }
}
