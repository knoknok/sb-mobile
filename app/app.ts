import 'es6-shim';
import {App, IonicApp, Platform, MenuController} from 'ionic-angular';
import {StatusBar, Push} from 'ionic-native';
import {HomePage} from './pages/home/home'
import {BellData} from './providers/bell-data/bell-data';
import {RingData} from './providers/ring-data/ring-data';

@App({
  templateUrl: 'build/app.html',
  config: {}, // http://ionicframework.com/docs/v2/api/config/Config/
  providers: [BellData, RingData]
})
class MyApp {
  // make HelloIonicPage the root (or first) page
  rootPage: any = HomePage;

  constructor(
    private app: IonicApp,
    private platform: Platform,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();

      // let push = Push.init({
      //   android: {
      //     senderID: "knoknok-1298"
      //   },
      //   ios: {
      //     alert: "true",
      //     badge: true,
      //     sound: 'false'
      //   },
      //   windows: {}
      // });
      //
      // push.on('registration', (data) => {
      //   console.log(data.registrationId);
      // });
      //
      // push.on('notification', (data) => {
      //   console.log(data.message);
      //   console.log(data.title);
      //   console.log(data.count);
      //   console.log(data.sound);
      //   console.log(data.image);
      //   console.log(data.additionalData);
      // });
      //
      // push.on('error', (e) => {
      //   console.log(e.message);
      // });

    });
  }
}
